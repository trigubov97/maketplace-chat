import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { WebsocketService } from './services/websocket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private socketService: WebsocketService,
    private snackbar: MatSnackBar,
  ){}

  ngOnInit(): void {
    this.socketService.connectWebSocket().subscribe(
      event => {
        console.log('TCL: NavBarComponent -> event', event);
        if (event.event === 'new_message') {
          this.showMessage(`Новое сообщение из чата: ${event.data.chatId}`);
        }
        const notifications = this.socketService.$notificationsSubject.getValue();
        notifications.push(event.data);
        this.socketService.$notificationsSubject.next(notifications);
      },
      err => {
        console.error(err);
        this.showMessage(`WS Error: ${err}`);
      },
      () => {
        console.log('websocket connection closed');
      }
    );
  }

  private showMessage(message: any) {
    this.snackbar.open(message, 'OK', { duration: 5000, verticalPosition: 'top' });
  }

}
