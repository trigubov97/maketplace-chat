import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { ChatMessage, ChatInfo, NewChatModel, MessageModel } from '../models/chat.model';
import { map } from 'rxjs/operators';
import { UserBaseModel } from '../models/user';
import { WebsocketService } from './websocket.service';

// tslint:disable: no-use-before-declare
export interface Message {
  author: string;
  message: string;
}

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  private CHAT_URL = 'ws://echo.websocket.org/';

  constructor(private wsService: WebsocketService, private http: HttpClient) { }

  public sendMessage(message) {
    return of({ message: `Send ${message}` });
  }

  getChats(): Observable<ChatInfo[]> {
    return of(chatList);
    // return this.http.get<ChatModel[]>('/api2dev/support/requests');
  }

  getChat(id: string): Observable<ChatInfo> {
    for (const el of chatList) {
      if (id === el.id) {
        return of(el);
      }
    }

    // return this.http.get<ChatModel>('/api2dev/support/requests/' + id);
  }

  getMessages(id: string): Observable<ChatMessage[]> {
    if (+id === 1) {
      return of(messages1);
    } else {
      return of(messages2);
    }

    // return this.http.get<CommentsModel>('/api2dev/support/requests/' + id + '/comments/');
  }

  createChat(chat: NewChatModel): Observable<any> {
    return of([]);

    // // TO-DO поменять httpOptions на private
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     Authorization: 'Bearer ' + this.auth.getToken()
    //   }),
    // };

    // return this.http.post<SupportTicket>('/api2dev/support/requests', ticket, httpOptions);
  }

  sendNewMessage(comment: ChatMessage) {

    this.wsService.sendEvent(comment);
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     Authorization: 'Bearer ' + this.auth.getToken(),
    //     'Content-Type': 'application/json',
    //   }),
    // };

    // const question = { comment: comment };
    // console.log('TCL: TicketsService -> question', question)

    // return this.http.put<any>('/api2dev/support/requests/' + id, question, httpOptions);
  }

  uploadMessageFiles(files: File[]): Observable<any> {
    return of([]);

    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     Authorization: 'Bearer ' + this.auth.getToken()
    //   }),
    // };

    // const formData = new FormData();
    // // formData.append('uploads', files[0]);
    // for (const el of files) {
    //   formData.append('uploads', el);
    // }
    // return this.http.post<any>('/api2dev/support/uploads', formData, httpOptions);
  }
}

const chatList: ChatInfo[] = [
  {
    id: '1',
    companyName: ['ООО Родина'],
    description: 'Поставка тканей',
    topic: 'Платье с подогревом',
    status: 'open',
    updatedAt: 1576454978454,
    createdAt: 1576454878654,
  },
  {
    id: '2',
    companyName: ['ОАО Сказка'],
    description: 'Отгрузка товаров',
    topic: 'Велосипед на паровом двигателе',
    status: 'closed',
    updatedAt: 1578495878434,
    createdAt: 1576465878834,
  },
];

const userOne: UserBaseModel = {
  userId: '11',
  email: '11mail@company.com',
};

const userOther: UserBaseModel = {
  userId: '1',
  email: 'evgentrigub@market.com',
};

const messages1: ChatMessage[] = [
  {
    message: 'Привет',
    createdAt: 1576454878434,
    userId: '1',
    chatId: '1',
  },
  {
    message: 'Че не отвечаешь?',
    createdAt: 1576454878834,
    userId: '1',
    chatId: '1',
  },
  {
    message: 'Ты мне не нравишься',
    createdAt: 1576454879334,
    userId: '2',
    chatId: '1',
  },
  {
    message: 'Ты мне тоже!',
    createdAt: 1576454879354,
    userId: '1',
    chatId: '1',
  },
];

const messages2: ChatMessage[] = [
  {
    message: 'Когда поставки?',
    createdAt: 1576454878434,
    userId: '1',
    chatId: '2',
  },
  {
    message: 'Есть несколько деталей, которые надо уточнить',
    createdAt: 1576454878834,
    userId: '1',
    chatId: '2',
  },
  {
    message: 'Поставки товара по плану в понельник',
    createdAt: 1576454879334,
    userId: '2',
    chatId: '2',
  },
  {
    message: 'Детали мне уже прислал Михаил Романович',
    createdAt: 1576454879354,
    userId: '2',
    chatId: '2',
  },
];
