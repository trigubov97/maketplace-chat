import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject, of } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { IWSMessage } from '../models/socket.model';
import { ChatMessage } from '../models/chat.model';

// export interface WebSocketToken {
//   wsToken: string;
// }

@Injectable({
  providedIn: 'root',
})
export class WebsocketService {
  private $myWebSocket: WebSocketSubject<IWSMessage>;

  $wsMessages: Observable<IWSMessage>;
  $notificationsSubject: BehaviorSubject<ChatMessage[]> = new BehaviorSubject([]);

  constructor(private auth: AuthService, private http: HttpClient) { }

  connectWebSocket(): Observable<IWSMessage> {
    return this.getWebSocketToken()
      .pipe(
        switchMap(token => this.getEventsFromWebSocket(token))
      );
  }

  disconnectWebSocket(): void {
    this.$myWebSocket.unsubscribe();
  }

  sendEvent(wsData: ChatMessage) {
    this.$myWebSocket.next({
      event: 'new_message',
      data: wsData
    });
  }

  private getWebSocketToken(): Observable<string> {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.auth.getToken(),
      }),
    };

    return of('token');
    // return this.http.get<WebSocketToken>(`${environment.apiUrlGo}/chat/token`, httpOptions);
  }

  private getEventsFromWebSocket(wsToken: string): Observable<IWSMessage> {
    // const url = `wss://address.com:1234${environment.apiUrlGo}/chat/?token=${wsToken}`;
    const url = 'ws://echo.websocket.org/';
    this.$myWebSocket = webSocket(url);
    return this.$myWebSocket.asObservable();
  }
}
