import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { UserViewModel, UserDTO } from '../models/user';

// tslint:disable: no-use-before-declare
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<UserViewModel>;
  public currentUser: Observable<UserViewModel>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<UserViewModel>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): UserViewModel {
    return this.currentUserSubject.value;
  }

  login(user: UserDTO): Observable<UserViewModel> {
    const userView: UserViewModel = userViewMock;
    localStorage.setItem('currentUser', JSON.stringify(userView));
    localStorage.setItem('authToken', 'res.accessToken');
    this.currentUserSubject.next(userView);
    return of(userView);

    // TO-DO раскоменнтировать

    // const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    // return this.http
    //   .post<TokenModel>(`${environment.apiUrlGo}/token`,
    //   `grant_type=password&login=${user.email}&password=${user.password}`, { headers })
    //   .pipe(
    //     catchError(this.handleError),
    //     switchMap(res => {
    //       const result = res && res.accessToken;
    //       if (!result) {
    //         return;
    //       }

    //       return this.getUserInfo(res.accessToken).pipe(
    //         catchError(this.handleError),
    //         tap(userView => {
    //           localStorage.setItem('currentUser', JSON.stringify(userView));
    //           localStorage.setItem('authToken', res.accessToken);
    //           this.currentUserSubject.next(userView);
    //         })
    //       );
    //     })
    //   );
  }

  getUserInfo(token: string): Observable<UserViewModel> {
    return (of(userViewMockConfirmed));

    // TO-DO раскоменнтировать

    // const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    // return this.http.get<UserViewModel>(`${environment.apiUrlGo}/user`, { headers }).pipe(catchError(this.handleError));
  }

  getToken(): string {
    return localStorage.getItem('authToken');
  }


  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  private handleError(error: HttpErrorResponse) {
    const msg = error.message + ` Status Code: ${error.status}`;
    console.error('AuthenticationService::handleError() ' + msg);
    return throwError('Error: ' + msg);
  }
}

const userViewMockConfirmed: UserViewModel = {
  userId: '1',
  surname: 'Бонд',
  name: 'Джеймс',
  patronymic: 'Евгеньевич',
  userName: 'bond007',
  email: 'trigubov97@gmail.com',
  emailConfirmed: true,
  createdDate: '2019-12-25T00:00:00Z',
};

const userViewMock: UserViewModel = {
  userId: '1',
  surname: '',
  name: '',
  patronymic: '',
  userName: '',
  email: 'trigubov97@gmail.com',
  emailConfirmed: false,
  createdDate: '2019-12-25T00:00:00Z',
};
