import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { UserDTO } from '../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  readonly loginForm: FormGroup;
  isLoading = false;
  hidePassword = true;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthService,
    private snackbar: MatSnackBar
  ) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/chat']);
    }
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  /**
   * Check validation for authentication
   */
  canSubmit(): boolean {
    return this.loginForm.valid;
  }

  /**
   * Sumbit the login form for authentication
   */
  onSubmit(): void {
    if (!this.canSubmit()) {
      return;
    }

    if (this.loginForm.invalid) {
      return;
    }
    const user = this.loginForm.value as UserDTO;
    this.isLoading = true;
    setTimeout(() => {
      this.authenticationService
        .login(user)
        .pipe(first())
        .subscribe(
          userView => {
            this.isLoading = false;
            const message = userView.emailConfirmed ? 'С возвращением!' : 'Завершите регистрацию для доступа ко всем возможностям';
            this.router.navigate(['/chat']);
            this.showMessage(message);
          },
          error => {
            this.isLoading = false;
            this.showMessage(error);
          }
        );
    }, 1500);
  }

  private showMessage(message: any) {
    this.snackbar.open(message, 'OK', { duration: 5000 });
  }
}
