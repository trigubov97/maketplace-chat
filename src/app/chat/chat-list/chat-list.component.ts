import { Component, OnInit, OnDestroy, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ChatInfo } from 'src/app/models/chat.model';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss'],
})
export class ChatListComponent implements OnInit, OnDestroy, OnChanges {
  @Input() orderType: string;
  @Input() orderDirection: string;
  @Input() refreshEvent: Observable<void>;

  @Output() chatSelected = new EventEmitter<ChatInfo>();

  readonly $destroyed: Subject<void> = new Subject<void>();
  private order = '';
  private direction = '';

  chatList: ChatInfo[];
  activeChat: ChatInfo;

  constructor(private chatService: ChatService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    const routeId = this.route.snapshot.paramMap.get('id');
    const id = routeId ? routeId : null;
    if (id) {
      this.chatService.getChat(id).subscribe(res => {
        this.activeChat = res;
        this.onChatSelected(res);
        this.loadChats(true);
      });
    } else {
      this.loadChats(false);
    }

    this.refreshEvent.pipe(takeUntil(this.$destroyed)).subscribe(_ => {
      this.loadChats(false);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    const ord = changes.orderType;
    const dir = changes.orderDirection;

    if (ord && ord.currentValue) {
      this.order = ord.currentValue;
    }
    if (dir && dir.currentValue) {
      this.direction = dir.currentValue;
    }

    this.sortChatList(this.order, this.direction);
  }

  ngOnDestroy(): void {
    this.$destroyed.next();
    this.$destroyed.complete();
  }

  onChatSelected(chat: ChatInfo): void {
    this.activeChat = chat;
    this.router.navigate([`chat`, chat.id]);
    this.chatSelected.emit(chat);
  }

  /**
   * Load chat list
   * @param isChatId chat Id form URL
   */
  private loadChats(isChatId: boolean): void {
    this.chatService
      .getChats()
      .pipe(takeUntil(this.$destroyed))
      .subscribe(res => {
        this.chatList = res;
        this.sortChatList(this.order, this.direction);

        if (!isChatId) {
          this.onChatSelected(this.chatList[0]);
        }
      });
  }

  private sortChatList(order: string, direction: string): void {
    if (!this.chatList) {
      return;
    }

    direction === 'asc'
      ? this.chatList.sort((a, b) => (a[order] > b[order] ? 1 : b[order] > a[order] ? -1 : 0))
      : this.chatList.sort((a, b) => (a[order] < b[order] ? 1 : b[order] < a[order] ? -1 : 0));
  }
}
