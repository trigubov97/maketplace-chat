import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';
import { MatDialog, MatSnackBar } from '@angular/material';
import { takeUntil, tap, switchMap } from 'rxjs/operators';
import { NewChatDialogComponent } from './new-chat-dialog/new-chat-dialog.component';
import { ChatModel, ChatInfo, NewChatModel, ChatMessage } from '../models/chat.model';
import { UserViewModel } from '../models/user';
import { ChatService } from '../services/chat.service';
import { AuthService } from '../services/auth.service';

interface OrderOptions {
  name: string;
  option: string;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit, OnDestroy {
  get direction(): string {
    return this.orderDirection;
  }

  set direction(newDir: string) {
    this.orderDirection = newDir;
  }

  readonly $destroyed: Subject<void> = new Subject<void>();

  $chatInfo: Observable<ChatModel>;

  orderDirection = 'desc';
  orderOptions: OrderOptions[] = [
    {
      name: 'Тема',
      option: 'topic',
    },
    {
      name: 'Дата',
      option: 'createdAt',
    },
  ];

  currentUser: UserViewModel;
  refreshChatsSubject: Subject<void> = new Subject<void>();

  constructor(private chatService: ChatService, private authService: AuthService, public dialog: MatDialog, private snackbar: MatSnackBar) {
    this.authService.currentUser.pipe(takeUntil(this.$destroyed)).subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit(): void { }

  ngOnDestroy(): void {
    this.$destroyed.next();
    this.$destroyed.complete();
  }

  chatSelected(chat: ChatInfo) {
    this.$chatInfo = this.getChatInfo(chat) as Observable<ChatModel>;
  }

  openNewChatDialog() {
    const dialogRef = this.dialog.open(NewChatDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe((result: NewChatModel) => {
      this.createChat(result);
    });
  }

  private getChatInfo(chat: ChatInfo): Observable<ChatModel | ChatMessage[]> {
    return this.chatService.getMessages(chat.id).pipe(
      tap(res => {
        res.sort((a, b) => (a.createdAt < b.createdAt ? 1 : b.createdAt < a.createdAt ? -1 : 0));
      }),
      switchMap(res => {
        const newChatInfo: ChatModel = {
          chat,
          messages: res.reverse(),
        };
        return of(newChatInfo);
      })
    );
  }

  private createChat(newchat: NewChatModel) {
    if (!newchat) {
      return;
    }

    this.chatService
      .createChat(newchat)
      .pipe(takeUntil(this.$destroyed))
      .subscribe(
        _ => {
          this.showMessage('Chat was added.');
          this.refreshChatsSubject.next();
        },
        error => {
          this.showMessage(error);
        }
      );
  }

  private showMessage(message: any) {
    this.snackbar.open(message, 'OK', { duration: 5000 });
  }
}
