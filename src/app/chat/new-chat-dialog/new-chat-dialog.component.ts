import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

interface NewTicketForm {
  subject: string;
  description: string;
}

@Component({
  selector: 'app-new-chat-dialog',
  templateUrl: './new-chat-dialog.component.html',
  styleUrls: ['./new-chat-dialog.component.scss'],
})
export class NewChatDialogComponent {
  readonly formChat: FormGroup;

  constructor(public dialogRef: MatDialogRef<NewChatDialogComponent>, private formBuilder: FormBuilder) {
    this.formChat = this.getTicketForm();
  }

  /**
   * Check expession for disabling submit button
   */
  canCreate() {
    return this.formChat.valid;
  }

  /**
   * Create NewTicket object and close dialog
   */
  onCreate() {
    if (!this.formChat) {
      return;
    }
    const val = this.formChat.value as NewTicketForm;
    this.dialogRef.close();
  }

  /**
   * Return ticket FormGroup
   */
  private getTicketForm(): FormGroup {
    return this.formBuilder.group({
      subject: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', [Validators.required, Validators.minLength(3)]],
    });
  }
}
