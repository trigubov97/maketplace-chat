import { Component, Input, ViewChild, ElementRef, OnChanges, SimpleChanges, OnInit, AfterViewChecked, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FileValidator } from 'ngx-material-file-input';
import { takeUntil, tap } from 'rxjs/operators';
import { ChatModel, UploadResponse, ChatMessage, Attachment } from 'src/app/models/chat.model';
import { UserViewModel } from 'src/app/models/user';
import { ChatService } from 'src/app/services/chat.service';
import { AuthService } from 'src/app/services/auth.service';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-messages-view',
  templateUrl: './messages-view.component.html',
  styleUrls: ['./messages-view.component.scss'],
})
export class MessagesViewComponent implements OnInit, AfterViewChecked, OnChanges, OnDestroy {
  // @ViewChild('chat') chatContainer: ElementRef;

  @Input() chatInfo: ChatModel | null;

  readonly $destroyed: Subject<void> = new Subject<void>();
  readonly maxSize = 20 * 2 ** 20; // 10 Mb
  readonly formMessage: FormGroup;
  currentUser: UserViewModel;

  isMessageSending: boolean;

  constructor(
    private chatService: ChatService,
    private formBuidler: FormBuilder,
    private authService: AuthService,
    private socketService: WebsocketService
  ) {
    this.authService.currentUser.subscribe(res => {
      this.currentUser = res;
    });
    this.formMessage = this.createMessageForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.chatInfo) {
      const chatInfo = changes.chatInfo.currentValue as ChatModel;

      if (!chatInfo) {
        return;
      }

      chatInfo && chatInfo.chat.status === 'closed' ? this.formMessage.disable() : this.formMessage.enable();
    }
  }

  ngOnInit(): void {
    this.socketService.$notificationsSubject.pipe(takeUntil(this.$destroyed)).subscribe(events => {
      if (!this.chatInfo) {
        return;
      }
      const isEventsInCurrentTicket = events.some(el => el.chatId === this.chatInfo.chat.id);
      if (!isEventsInCurrentTicket) {
        return;
      }

      this.handleMessagesInCurrentChat(events);

      const notificationsArray = events.filter(item => item.chatId !== this.chatInfo.chat.id);
      this.socketService.$notificationsSubject.next(notificationsArray);
    });
  }

  ngAfterViewChecked(): void {
    this.scrollToBottom();
  }

  ngOnDestroy(): void {
    this.$destroyed.next();
    this.$destroyed.complete();
  }

  canSend(): boolean {
    return this.formMessage.valid && this.formMessage.value.message && this.formMessage.value.message.length > 2;
  }

  onSendMessage(): void {
    const message = this.formMessage.value.message;
    const files = this.formMessage.value.attachments ? (this.formMessage.value.attachments.files as Array<File>) : null;

    this.isMessageSending = true;

    // TO-DO chat attachments
    // if (files && files.length > 0) {
    //   this.chatService
    //     .uploadMessageFiles(files)
    //     .pipe(takeUntil(this.$destroyed))
    //     .subscribe(
    //       response => {
    //         this.sendMessage(message, response);
    //       },
    //       error => { }
    //     );
    // } else {
    //   this.sendMessage(message);
    // }
    this.sendMessage(message);

    this.formMessage.reset();
  }

  sendMessage(text: string, filesResponse: UploadResponse = null): void {
    const chatId = this.chatInfo.chat.id;
    // TO-DO chat attachments
    // const newMessage: MessageModel = {
    //   body: text,
    //   uploads: [],
    // };

    const chatMesage: ChatMessage = {
      // message: newMessage,
      message: text,
      createdAt: Math.round(new Date().getTime() / 1000),
      userId: '1',
      chatId
    };

    // TO-DO chat attachments
    // if (filesResponse) {
    //   comment.uploads = [filesResponse.token];
    //   chatMesage.attachments = filesResponse.attachments;
    // }

    // this.chatInfo.messages.push(chatMesage);

    // this.chatService.sendNewMessage(chatMesage, chatId);
    // .subscribe(res => {
    //   this.isMessageSending = false;
    //   this.formMessage.markAsPristine();
    // });
    this.chatService.sendNewMessage(chatMesage);
    this.chatInfo.messages.push(chatMesage)
    this.isMessageSending = false;
    this.formMessage.markAsPristine();
  }

  onFileClicked(file: Attachment) {
    window.open(file.content_url);
  }

  private handleMessagesInCurrentChat(events: ChatMessage[]): void {
    const messages: ChatMessage[] = [];

    events.forEach(el => {
      if (el.chatId === this.chatInfo.chat.id && el.message) {
        messages.push(el);
      }
    });
    if (messages.length === 0) {
      return;
    }
    messages.forEach(el => {
      this.chatInfo.messages.push(el);
    });
  }

  private scrollToBottom = () => {
    try {
      // this.chatContainer.nativeElement.scrollTop = this.chatContainer.nativeElement.scrollHeight;
    } catch (err) { }
    // tslint:disable-next-line:semicolon
  };

  private createMessageForm(): FormGroup {
    return this.formBuidler.group({
      message: ['', [Validators.nullValidator, Validators.minLength(3)]],
      attachments: [null, [FileValidator.maxContentSize(this.maxSize)]],
    });
  }
}
