import { UserAuthModel } from './user';

export interface ChatModel {
  chat: ChatInfo;
  messages: ChatMessage[];
}

export interface ChatInfo {
  id: string;
  companyName: string[];
  description: string;
  topic: string;
  status: string;
  updatedAt: number;
  createdAt: number;
  // icon?: Blob;
}

export interface ChatMessage {
  chatId: string;
  userId: string;
  createdAt: number;
  message: string;
  // message: MessageModel;
  // attachments?: Attachment[];
}

export interface MessageModel {
  body: string;
  uploads: string[];
}

export interface NewChatModel {
  user: UserAuthModel;
}

export interface UploadResponse {
  token: string;
  attachments: Attachment[];
}

export interface Attachment {
  file_name: string;
  content_type: string;
  content_url: string;
  mapped_content_url: string;
  thumbnails: [
    {
      content_url: string;
    }
  ];
}
