import { ChatMessage } from './chat.model';

export type EventTypeFromWS = 'new_message';

export interface IWSMessage {
  event: EventTypeFromWS;
  data: ChatMessage;
}

// export interface ChatMessageWS {
//   ticket: number;
//   comment: string;
// }
