﻿export class UserDTO {
  email: string;
  password: string;
}

export class UserBaseModel {
  userId: string;
  email: string;
}

export class UserViewModel extends UserBaseModel {
  userName: string;
  name: string;
  surname: string;
  patronymic: string;
  createdDate: string;
  emailConfirmed: boolean;
}

export class UserAuthModel extends UserViewModel {
  accessToken: string;
}
